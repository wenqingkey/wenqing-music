import App from './App'
// 字体图标
import '@/common/iconfont.css'
// 工具函数
import '@/common/utils.js'
// 全局状态
import store from '@/store/index.js'

import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App,
	store
})
app.$mount()

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif