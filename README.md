# wenqingMusic

本项目使用`uniapp` + `node.js`构建。  
本项目仅用于学习。  
如果对你有帮助可以随手点个star

## 更新日志

- 2022-08-05

1. 新增主页轮播图

- 2022-08-04

1. 新增登录功能-密码/验证码登录

## 关于后端

### node后台

[网易云音乐 Binaryify/NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi)

### 接口文档

[网易云音乐 NodeJS 版 API](https://binaryify.github.io/NeteaseCloudMusicApi/#/)

## 参考项目

本项目参考`千锋uni-app框架实战网易云音乐项目`并在其基础上做了优化

1. 解决榜单列表页与歌曲播放页filter背景模糊白边问题
2. 解决歌曲播放页一些歌曲封面比较白、亮作为背景会导致页面文字不清晰的问题
3. 优化很多地方文本溢出问题(包括各页面歌名，歌词、页面标题......)
4. 优化歌曲多个歌手显示问题
5. 优化歌单播放完成最后一首歌与搜索歌曲播放完成bug问题
6. 新增播放/暂停时摇杆动画效果
7. 使用setTimeout替换setInterval([为什么要用 setTimeout 替换 setInterval](https://juejin.cn/post/6844903971962765326))
8. 使用scss替换css(个人习惯)
9. 新增歌曲搜索框进入页面与清除内容自动聚焦
10. 新增热评与最新评论切换显示功能
11. 解决微信小程序端返回上一页/返回主页按钮组与小程序自带的胶囊按钮对齐问题，比如刘海屏、水滴屏会有错位
12. 新增搜索框防抖实现

## 在线预览

[温情云音乐](https://wenqing-music-0giut48m32d1124d-1311534113.ap-shanghai.app.tcloudbase.com/#/)

## 效果展示

### 首页

![首页](https://img-blog.csdnimg.cn/8ec463a545974242949a94b36446c25e.png)

### 排行榜详情页

![排行榜详情页](https://img-blog.csdnimg.cn/3b541b4f21124745be99224af8a64382.png)

### 歌曲播放页

<table>
    <tr>
        <td><img style="height: 500px" src="https://img-blog.csdnimg.cn/39450a90a8004d639e40ae37e8dd7217.png"  暂停 </td>
        <td><img style="height: 500px"  src="https://img-blog.csdnimg.cn/80fd88c5672443dfb577fe3f0b099a4b.png"  > 播放 </td>
    </tr>
    <tr>
        <td><img style="height: 500px"  src="https://img-blog.csdnimg.cn/7a17b42153564362933ed31d9298decf.png"  > 最热 </td>
        <td><img style="height: 500px"  src="https://img-blog.csdnimg.cn/5653dd8d13f64079ab2c18b93c6d3925.png"  最新 </td>
    </tr>
</table>

### 搜索页

<table>
    <tr>
        <td><img style="height: 400px"  src="https://img-blog.csdnimg.cn/00ded00019a3416e8cfaa573a6dbcab0.png"  搜索 </td>
        <td><img style="height: 400px"  src="https://img-blog.csdnimg.cn/fd15a68916f546db88efacad4dd03c90.png"> 提示 </td>
        <td><img style="height: 400px"  src="https://img-blog.csdnimg.cn/00da7a3d93924366afcc62ac7a1524be.png"> 结果 </td>
    </tr>
</table>

## 运行项目

```bash
git clone https://gitee.com/wenqingkey/wenqing-music.git

npm install
```

因为是 uniapp 项目所以介意使用 HBuilder 直接在工具栏->运行中运行

## 联系我

如有疑问

<img style="width: 300px" src="https://img-blog.csdnimg.cn/352345ecc2e94d56a46c3d849752c415.png"></img>