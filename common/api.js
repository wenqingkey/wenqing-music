import {
	baseUrl
} from './config.js'

/* 手机号密码登录 */
export function loginForPwd(phone, password) {
	return new Promise((resolve, reject) => {
		uni.request({
			url: `${baseUrl}/login/cellphone?phone=${phone}&password=${password}`,
			method: 'GET',
			success: (res) => {
				uni.setStorage({
					key: 'cookie',
					data: res.data.cookie.split(';;').find(item => item.indexOf('MUSIC_U') !== -1)
				});
				resolve(res);
			},
		})
	})
}

/* 发送验证码 */
export function sendCodeApi(phone) {
	return uni.request({
		url: `${baseUrl}/captcha/sent?phone=${phone}`,
		method: 'GET'
	})
}

/* 验证码登录 */
export function loginForCode(phone, code) {
	return uni.request({
		url: `${baseUrl}/login/cellphone?phone=${phone}&captcha=${code}`,
		method: 'GET',
	})
}

/* 轮播图 */
export function getBanner() {
	return uni.request({
		url: `${baseUrl}/banner?type=1`,
		method: 'GET',
	})
}

/* 播放歌曲记录 */
export function getLatelyPlay(id) {
	return new Promise((resolve, reject) => {
		uni.request({
			url: `${baseUrl}/user/record?uid=${id}&type=0`,
			method: 'GET',
			success: (res) => {
				const result = res.data.allData.slice(0, 15);
				resolve(result);
			}
		})
	})
}

/* 获取用户等级 */
export function getUserLevel(id) {
	return uni.request({
		url: `${baseUrl}/user/level?uid=${id}`,
		method: 'GET',
	})
}

/* 全部榜单排行榜 */
export function topList() {
	return new Promise((resolve, reject) => {
		uni.request({
			url: `${baseUrl}/toplist/detail`,
			method: 'GET',
			success: res => {
				let topArr = res.data.list.slice(0, 4);
				const result = {
					code: res.data.code,
					list: topArr
				};
				// console.log(result);
				resolve(result);
			}
		})
	})
}

/* 单个歌单排行榜详情 */
export function playListDetail(id) {
	return uni.request({
		url: `${baseUrl}/playlist/detail?id=${id}`,
		method: 'GET'
	})
}

/* 获取歌曲详情 */
export function getMusicDetail(id) {
	return uni.request({
		url: `${baseUrl}/song/detail/?ids=${id}`,
		method: 'GET'
	})
}

/* 获取相似歌曲 */
export function getSimiSong(id) {
	return uni.request({
		url: `${baseUrl}/simi/song?id=${id}`,
		method: 'GET'
	})
}

/* 获取歌词信息 */
export function getSongLyric(id) {
	return uni.request({
		url: `${baseUrl}/lyric?id=${id}`,
		method: 'GET'
	})
}

/* 获取歌曲评论 */
export function getSongComment(id) {
	return uni.request({
		url: `${baseUrl}/comment/music?id=${id}&limit=50`,
		method: 'GET'
	})
}

/* 歌曲地址 */
export function getMusicUrl(url) {
	return uni.request({
		url: `${baseUrl}/song/url?id=${url}`,
		method: 'GET'
	})
}

/* 获取热搜 */
export function getSearchHot() {
	return uni.request({
		url: `${baseUrl}/search/hot/detail`,
		method: 'GET'
	})
}

/* 搜索结果 */
export function getSearchResultApi(word) {
	return uni.request({
		url: `${baseUrl}/search?keywords=${word}`,
		method: 'GET'
	})
}

/* 搜索提示 */
export function getSearchSuggest(word) {
	return uni.request({
		url: `${baseUrl}/search/suggest?keywords=${word}&type=mobile`,
		method: 'GET'
	})
}

/* 歌单全部歌曲 */
export function getSongSheltAllList(id) {
	return uni.request({
		url: `${baseUrl}/playlist/track/all?id=${id}`,
		method: 'GET'
	})
}
