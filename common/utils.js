import Vue from 'vue';

Vue.filter('formatNum', (val) => {
	if(val >= 10000 && val < 100000000) {
		val /= 10000;
		return val.toFixed(1) + '万';
	} else if(val >= 100000000) {
		val /= 100000000;
		return val.toFixed(1) + '亿';
	} else {
		return val;
	}
})