import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		// 播放列表id数组
		topListIds: [],
		// 下一首歌id
		nextId: undefined,
	},
	mutations: {
		/* 重置播放列表id数组 */
		init_topListIds(state, idArr) {
			state.topListIds = idArr;
		},
		/* 下一首歌曲id */
		get_next_id(state, id) {
			state.topListIds.forEach((item, index, arr) => {
				if (item.id === Number(id)) {
					if (index === arr.length - 1) {
						state.nextId = arr[0].id;
					} else {
						state.nextId = arr[index + 1].id;
					}
				}
			})
			console.log('下一首歌曲id', state.nextId);
		}
	},
})
